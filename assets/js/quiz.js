var series = {};
series.vowels = ["a","i","u","e","o"];
series.k = ["ka","ki","ku","ke","ko","ga","gi","gu","ge","go"];
series.s = ["sa","shi","su","se","so","za","ji","zu","ze","zo"];
series.t = ["ta","chi","tsu","te","to","da","ji","zu","de","do"];
series.n = ["na","ni","nu","ne","no"];
series.h = ["ha","hi","fu","he","ho","ba","bi","bu","be","bo","pa","pi","pu","pe","po"];
series.m = ["ma","mi","mu","me","mo"];
series.r = ["ra","ri","ru","re","ro"];
series.y = ["ya","yu","yo"];
series.w = ["wa","wo","n"];

var limit;
var answer;
var quizType;
var remainingChars;
var keys = Object.keys(series);

function startQuiz(){
	remainingChars = [];
	quizType = $("#quizzes option:selected").val();
	if(quizType == "all"){ 
		for(var i in series){ 
			remainingChars[i] = series[i]; 
		}
	} else { 
		remainingChars = series[quizType]; 

		var tentenMaru = $("#tentenMaru").is(":checked");
		if(tentenMaru && (quizType == "k" || quizType == "s" || quizType == "t")) remainingChars = remainingChars.slice(0,10);
		else if(tentenMaru && quizType == "h") remainingChars = remainingChars.slice(0,15);
		else if(quizType == "w" || quizType == "y") remainingChars = remainingChars.slice(0,3);
		else remainingChars = remainingChars.slice(0,5);
	}
	askQuestion();
}

function askQuestion(){
	if ($("#unending").is(":checked") && remainingChars.length == 0){ startQuiz(); } 
	else if(remainingChars.length == 0){ $("#charToDisplay").attr("src","assets/img/finished.png"); }
	
	if(quizType == "all"){
		if(keys.length > 0){
			var randKey = keys[getRandomInt(0, keys.length-1)];
			var randSeries = remainingChars[randKey];
			var randChar = randSeries[getRandomInt(0, randSeries.length-1)];

			$("#charToDisplay").attr("src", "assets/img/" + randKey + "/" + randChar + ".png");
			answer = randChar;
			
			remainingChars[randKey].splice(remainingChars[randKey].indexOf(randChar),1);
			if(remainingChars[randKey].length == 0){ keys.splice(keys.indexOf(randKey),1); }
		}
	} else {
		if(remainingChars.length > 0){
			var randChar = getRandomInt(0, remainingChars.length-1);

			$("#charToDisplay").attr("src", "assets/img/" + quizType + "/" + remainingChars[randChar] + ".png");
			answer = remainingChars[randChar];
			
			remainingChars.splice(randChar, 1);		
		}
	}
}

function checkAnswer(input){
	if(answer == input){
		$("input").val('');
		askQuestion();
	} else {
		//TODO error feedback
	}
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}