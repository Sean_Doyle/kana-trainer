## Kana Flashcard Quiz ##

### What is this repository for? ###

This is a small web app that tests you on your knowledge of the Kana.  
Currently only asks the Hiragana Characters.  
Future updates will include:  

1. Katakana.  
2. Kanji.  
3. Words and Phrases in all 3 syllabaries.  

### How do I get set up? ###

Simply open the index.html file with any respectable browser.
To answer type the romanji breakdown into the input field.

eg. あ = a, か = ka.